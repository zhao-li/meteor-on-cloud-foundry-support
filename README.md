# Meteor on Cloud Foundry Support
This example application is deployed to https://meteor-shower.app.cloud.gov/

Prerequisites
-------------
1. install docker
1. install docker-compose
1. install git
1. clone repository: `git clone --recursive https://gitlab.com/zhao-li/meteor-on-cloud-foundry-support.git`

Getting Started
---------------
1. start service: `docker-compose up`

Creating Application
====================
Use the `cf-cli` container to create a new Cloud Foundry Docker app.

    $ docker-compose run cf-cli bash
    cf-cli$ cf login -a api.fr.cloud.gov  --sso
    cf-cli$ cf create-app meteor-shower --app-type docker

Deploying
=========
Build the image and push to a docker registry that cloud.gov can access:

    $ docker login registry.gitlab.com
    $ docker build -t registry.gitlab.com/zhao-li/meteor-on-cloud-foundry-support .
    $ docker push registry.gitlab.com/zhao-li/meteor-on-cloud-foundry-support

Use cf cli to deploy:

    $ docker-compose run cf-cli bash
    cf-cli$ cf login -a api.fr.cloud.gov  --sso
    cf-cli% CF_DOCKER_PASSWORD=[access token] cf push meteor-shower --docker-image registry.gitlab.com/zhao-li/meteor-on-cloud-foundry-support --docker-username zhao-li

