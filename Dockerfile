FROM node:16.6.1-buster-slim as development
ARG APP_DIR=/usr/src/app/
WORKDIR ${APP_DIR}/

### https://stackoverflow.com/questions/59095541/how-to-install-meteor-on-alpine
ENV METEOR_VERSION=1.8.1
ENV LC_ALL=POSIX
ENV METEOR_ALLOW_SUPERUSER=1
RUN apt-get -yqq update \
  && DEBIAN_FRONTEND=noninteractive apt-get -yqq install \
    curl \
    g++ \
    make \
  && apt-get clean && rm -rf /var/lib/apt/lists/*
###:end https://stackoverflow.com/questions/59095541/how-to-install-meteor-on-alpine

RUN curl https://install.meteor.com/ | sh

COPY . ${APP_DIR}
RUN /usr/local/bin/meteor npm install

CMD scripts/start_app.sh

#FROM development as builder
#RUN scripts/build_app.sh
#
#
#FROM registry.access.redhat.com/ubi8/ubi-minimal as production
#ARG APP_DIR=/usr/src/app/
#WORKDIR ${APP_DIR}/
#
#RUN microdnf update && microdnf install \
#  && microdnf clean all # this will error out if no packages are listed above for installation
#
#COPY . ${APP_DIR}
